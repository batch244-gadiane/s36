//controllers - business logic
const Task = require("../models/task");

//controller for getting all tasks
module.exports.getAllTasks = () =>{
	return Task.find({}).then(result => {
		return result;
	})
}
//creation task
module.exports.createTask = (requestBody) => {

	let newTask = new Task ({
		name: requestBody.name
	})
	return newTask.save().then((task,error) =>{
		if (error){
			console.log (error);
			return false;
		}else {
			return task;
		}

	})

}

module.exports.deleteTask = (taskId) =>{
	return Task.findByIdAndRemove(taskId).then((removedTask,err)=> {

		if(err){
			console.log(err);
			return false;
		} else{
			return removedTask;
		}
	})
}

module.exports.updateTask =(taskId, newContent)=>{
	return Task.findById(taskId).then((result,error) =>{
		if (error){
			console.log(error);
			return false;
		} else{
			result.name = newContent.name;
			return result.save().then((updateTask,saveErr)=> {
				if (saveErr){
					console.log(saveErr)
					return false;
				}else {
					return updateTask;
				}
				
			})
		}
	})
}

//Activity

module.exports.getSpecificTask = (taskId) =>{
    return Task.findById(taskId).then(result => {
        return result;
    })
}

module.exports.changeStatus = (taskId, newStatus) =>{
    return Task.findById(taskId).then((result, error) => {
        if(error){
            console.log(error);
            return false;
        } else {
            result.status = newStatus.status;
            return result.save().then((updatedStatus, saveErr) => {
                if(saveErr){
                    console.log(saveErr);
                    return false;
                } else {
                    return updatedStatus;
                }
            })
        }
    })
}