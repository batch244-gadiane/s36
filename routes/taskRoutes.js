//routes - contains all the endpoints for our applications

const  express =  require ("express");

//router is for http methods
const router =  express.Router();

//route to get all the tasks
const taskController = require("../controllers/taskController");

router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));

});

//
router.post("/", (req,res)=>{
	taskController.createTask(req.body).then(resultFromController => res. send(resultFromController));

})

router.delete("/:id", (req,res)=>{
taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))

})

//
router.put("/:id", (req,res)=>{
taskController.updateTask(req.params.id,req.body).then(resultFromController => res.send(resultFromController));

})

//export the module

module.exports =  router;

//Activity

router.get("/:id", (req, res) => {
    taskController.getSpecificTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

router.put("/:id/complete", (req, res) => {
    taskController.changeStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});